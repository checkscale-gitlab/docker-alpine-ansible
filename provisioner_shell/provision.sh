#!/bin/sh
# set -e
# Provisions an Alpine Linux-based Docker container
# that can be used to test Ansible roles using Molecule
# (with Docker) and Tox.

# Installs the packages needed by the container.
install_packages() {
  echo "Installing OS packages..."
  apk update && apk add --no-cache \
                    automake \
                    build-base \
                    curl \
                    gcc \
                    git \
                    libffi-dev \
                    linux-headers \
                    musl-dev \
                    openssh \
                    openssl-dev \
                    py3-cryptography \
                    py3-pip \
                    python3-dev

  echo "Installing Python packages..."
  # The --ignore-installed flag is a necessary workaround
  # for a quirk with Alpine Linux. Without it, Tox will fail
  # to install properly.
  pip3 install --ignore-installed distlib \
       ansible~="${ANSIBLE_VERSION}" \
       docker~="${DOCKER_VERSION}" \
       molecule~="${MOLECULE_VERSION}" \
       molecule-docker~="${MOLECULE_DOCKER_VERSION}" \
       tox~="${TOX_VERSION}"
}

install_packages
