# Discussion
*This template is for issues that are related to discussing the project without actually making any changes.*

## What topic would you like to discuss?

/label ~"Category: Discussion" ~"Severity: 2"
