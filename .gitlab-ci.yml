---
# Global Config
# =============
# Global configuration settings for GitLab CI.

# Global Variables
# ----------------
# These are inherited by all GitLab CI jobs.
variables:
  # The version of Packer to install on the build
  # environment container so that Packer can be used
  # to build the Terragrunt container.
  PACKER_VERSION: '1.6.5'
  # Set the URL where Docker images should be pushed
  # to the project's Docker registry.
  PKR_VAR_docker_repository: "$CI_REGISTRY_IMAGE"
  # Sets the version of Docker to use in the build
  # pipeline.
  PKR_VAR_docker_version: '19.03'
  # The version of the 'ansible' Python package that
  # will be installed in the built container.
  PKR_VAR_python_ansible_version: '2.10'
  # The version of the 'docker' Python package that
  # will be installed in the built container.
  PKR_VAR_python_docker_version: '4.4'
  # The version of the 'molecule' Python package that
  # will be installed in the built container.
  PKR_VAR_python_molecule_version: '3.2'
  # The version of the 'molecule-docker' Python package
  # that will be installed in the built container.
  PKR_VAR_python_molecule_docker_version: '0.2'
  # The version of the 'tox' Python package that will
  # be installed in the built container.
  PKR_VAR_python_tox_version: '3.20'

# Stage Definitions
# -----------------
# All GitLab CI jobs must belong to one of these
# job stages.
stages:
  - lint
  - build

# Job Templates
# =============
# Defines common groups of settings for GitLab CI
# jobs.

# Provider Templates
# ------------------
# Common settings that should be inherited by all
# GitLab CI jobs that run on Docker runners.
.runner_docker:
  tags:
    - docker
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"

# Stage Templates
# ---------------
# Common settings that should be inherited by all
# GitLab CI jobs that run in the 'lint' stage.
.stage_lint:
  stage: lint

# Common settings that should be inherited by all
# GitLab CI jobs that run in the 'build' stage.
.stage_build:
  stage: build
  image:
    name: docker:19.03
  # Installs Packer so that it can be used to build and
  # provision the Terragrunt container.
  before_script:
    - apk add zip
    - cd /usr/local/bin
    - wget "https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip" -O packer.zip
    - unzip packer.zip
    - "cd -"
    # Log in to this project's GitLab registry using an ephemeral,
    # automatically-generated username and password.
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  # Set up a separate container that has a Docker
  # process running so that this stage's main
  # container can run 'Docker in Docker'.
  services:
    - docker:19.03-dind

# GitLab CI Jobs
# ==============
# Defines the jobs that will be executed in the
# GitLab CI pipeline.

# Lint Stage
# ----------
# A shell script linter. Used when the build scripts
# used to build this container change.
lint_shellcheck:
  extends:
    - .runner_docker
    - .stage_lint
  image: pipelinecomponents/shellcheck:latest
  script:
    - echo "Running Shellcheck shell script linter"
    - |
      find . -name .git -type d -prune -o -type f -name \*.sh -print0 |
      xargs -0 -r -n1 shellcheck
  rules:
    # Don't run if we're just adding a Git tag.
    # Linting should already have been performed.
    - if: '$CI_COMMIT_TAG'
      when: never
    - changes:
      - "**/*.sh"
      when: always
    - when: never

# A YAML syntax linter. Useful for when the
# GitLab CI file changes.
lint_yamllint:
  extends:
    - .runner_docker
    - .stage_lint
  image: pipelinecomponents/yamllint:latest
  script:
    - echo "Running yamllint YAML syntax linter"
    - yamllint .
  # Only lint the GitLab CI file if it changes.
  rules:
    # Don't run if we're just adding a Git tag.
    # Linting should already have been performed.
    - if: '$CI_COMMIT_TAG'
      when: never
    - changes:
      - .gitlab-ci.yml
      when: always
    - when: never

# Build Stage
# -----------
# Give users the option to manually trigger a Docker
# build for merge request commits.
#
# If triggered, the container is pushed to a path in
# the project registry tagged after the branch name.
build_testing:
  extends:
    - .runner_docker
    - .stage_build
  variables:
    PKR_VAR_docker_tag: "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
  script:
    - packer build .
  rules:
    # Only trigger for merge requests.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      # Only trigger if Packer files or shell scripts
      # have changed.
      changes:
        - .gitlab-ci.yml
        - "*.pkr.hcl"
        - "**/*.sh"
      when: manual
    - when: never

# Build the container and push it to the project's
# root-level Docker registry when a Git tag (used
# for versions) is attached.
build_production:
  extends:
    - .runner_docker
    - .stage_build
  variables:
    # Tag the Docker container with the tag that was
    # applied to the GitLab repository.
    PKR_VAR_docker_tag: "$CI_COMMIT_TAG"
  script:
    - packer build .
  rules:
    # If this is a tag commit, then run. Tags should
    # only be added to the 'master' branch.
    - if: '$CI_COMMIT_TAG'
      when: on_success
    # If it's not a tag commit, then don't run.
    - when: never

# Templates
# =========

# Prevent duplicate GitLab CI pipelines. Normally, when pushing to a
# branch associated with a merge request, the pipeline will run once
# in the pipeline and again for the merge request.
#
# This pre-built template only runs the pipeline for commits to the
# master branch, commits to tags, and merge request pipelines.
# Commits to branches that don't have a merge request are excluded,
# and if a branch does have a merge request, the pipeline won't be
# run twice.
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
